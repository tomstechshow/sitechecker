$data = Get-Content 'settings.ini'
foreach ($item in $data) {
$name,$value = $item -split '='
    switch ($name) {
        'description' { $description = $value }
        'site' { $site = $value }
        'checkfor' {$checkfor = $value }
        'email' { $email = $value }
        'pass' { $pass = $value }
        'smtp' { $smtp = $value }
    }
}
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$webpage = Invoke-WebRequest $site
$bits = $webpage.RawContentLength
$continue = "TRUE"
$bits

While ($continue -eq "TRUE") {
    Start-Sleep -s 300    
    $webpage = Invoke-WebRequest $site
    $newbits = $webpage.RawContentLength
    $newbits
    if ($newbits -ne $bits) {
        $Subject = "No-Alert"
        if ($webpage.RawContent | Select-String -Pattern $checkfor ) {
            $Subject = $description
        }
        Write-Host "CHANGE"
        $newbits
        if ($Subject -ne "No-Alert" ) {
            $EmailFrom = $email
            $EmailTo = $email
            $Body = "The page has Changed " +  $site
            $SMTPMessage = New-Object System.Net.Mail.MailMessage ($EmailFrom, $EmailTo,$Subject ,$Body)
            $SMTPServer = $smtp
            $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer , 587 )
            $SMTPClient.EnableSsl = $true
            $SMTPClient.Credentials = New-Object System.Net.NetworkCredential ($email, $pass)
            $SMTPClient.Send( $SMTPMessage)
        }
    }
    $bits = $newbits
} 